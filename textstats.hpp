
#ifndef TEXTSTATS_HPP_INCLUDED
#define TEXTSTATS_HPP_INCLUDED

#include <string>
#include <vector>
#include <unordered_set>
#include <map>
#include <set>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

// get_tokens разбивает текст, закодированный в ASCII, на слова,
// с переводом всех букв к нижнему регистру.
// Слово -- это последовательность символов, не являющихся
// разделителями.
void get_tokens(
	// [ВХОД] Текстовая строка.
	const string &s,
	// [ВХОД] Множество символов-разделителей.
	const unordered_set<char> &delimiters,
	// [ВЫХОД] Последовательность слов.
	vector<string> &tokens
);

// get_type_freq составляет частотный словарь текста, в котором
// для каждого слова указано количество его вхождений в текст.
void get_type_freq(
	// [ВХОД] Последовательность слов.
	const vector<string> &tokens,
	// [ВЫХОД] Частотный словарь
	// (ключи -- слова, значения -- количества вхождений).
	map<string, int> &freqdi
);

// get_types составляет список уникальных слов, встречающихся в
// тексте. Список должен быть отсортирован лексикографически.
void get_types(
	// [ВХОД] Последовательность слов.
	const vector<string> &tokens,
	// [ВЫХОД] Список уникальных слов.
	vector<string> &wtypes
);

// get_x_freq_words формирует лексикографически отсортированный
// список слов, взятых из частотного словаря, которые встречаются
// в тексте не меньше x раз.
void get_x_freq_words(
	// [ВХОД] Частотный словарь
	const map<string, int> &freqdi,
	// [ВХОД] Минимальное количество вхождений.
	int x,
	// [ВЫХОД] Список слов, встречающихся не меньше x раз.
	set<string> &words
);

// get_intersection формирует список ключевых слов, относящихся к данной тематике
void get_intersection(
	// [ВХОД] тематический словарь
	string src,
	// [ВХОД] ключевые слова
	set<string> &keywords,
	// [ВЫХОД]
	vector<string> &inter
);

void analyse_themes(set<string> &keywords);
void analyse_article(string src);

#endif
