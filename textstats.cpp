#include "textstats.hpp"
#include <algorithm>
#include <ctype.h>
#include <iterator>

// Множество разделителей слов в тексте.
const unordered_set<char> delimiters {
	'~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_',
	'+', '-', '=', '`', '{', '}', '[', ']', '|', '\\', ':', ';',
	'\"', '\'', ',', '.', '/', '<', '>', '?', ' ', '\t', '\n'
};

//Функция get_tokens разбивает текст, закодированный в ASCII, на слова с переводом всех букв к нижнему регистру. Слово – это последовательность символов, не являющихся разделителями.
void get_tokens(const string &s,const unordered_set<char> &delimiters, vector<string> &tokens)
{
    int i=0;
    while (i<s.size()){
        string str;
        while (i<s.size() && delimiters.count(s[i])!=0)
          i++;
        while (i<s.size() && delimiters.count(s[i])==0){
					if (!islower(s[i]))
            str+=(char)tolower(s[i++]);
          else str+=s[i++];
        }
				if (str!="" && str.length()>4)
        	tokens.push_back(str);
    }
}

//Функция get_type_freq составляет частотный словарь текста, в котором для каждого слова указано количество его вхождений в текст.
void get_type_freq(const vector<string> &tokens, map<string, int> &freqdi)
{
	for (auto s : tokens){
		if (freqdi.count(s)!=0)
			freqdi[s]++;
		else freqdi.insert(make_pair(s, 1));
	}
}

//Функция get_types составляет список уникальных слов, встречающихся в тексте. Список должен быть отсортирован лексикографически.
void get_types(const vector<string> &tokens, vector<string> &wtypes)
{
  unordered_set<string> s(tokens.begin(), tokens.end());
  copy(s.begin(), s.end(), inserter(wtypes, wtypes.end()));
  sort(wtypes.begin(), wtypes.end());
}

//Функция get_x_freq_words формирует лексикографически отсортированный список слов, взятых из частотного словаря, которые встречаются в тексте не меньше x раз.
void get_x_freq_words(const map<string, int> &freqdi, int x, set<string> &words)
{
	for (auto f : freqdi)
		if (f.second >= x)
			words.insert(f.first);
}

void get_intersection(string src,	set<string> &keywords, vector<string> &inter){
  ifstream fin(src);
  string word;
  set<string> dict;
  while(fin >> word)
    dict.insert(word);
  fin.close();
  set_intersection(dict.begin(), dict.end(), keywords.begin(), keywords.end(), inserter(inter, inter.begin()));
}

void analyse_themes(set<string> &keywords){
  ostream_iterator<string> owords(cout, " ");
  DIR *dir;
  struct dirent *entry;
  struct stat buf;
  dir = opendir("themes");
  string s = "themes/";
  map <string, int> stats;
  while ((entry=readdir(dir)) != NULL) {
    vector<string> inter;
    string name = entry->d_name;
    lstat(entry->d_name, &buf);
    if (name.length()>2) {
      string str = s+name;
      get_intersection(str.c_str(), keywords, inter);
      if (inter.size()>0){
				map<string, int>::iterator it=stats.begin();
				while (it !=stats.end() && it->second > inter.size()){
					it++;
				}
				stats.insert(it, make_pair(name.substr(0, name.length()-4), inter.size()));
      }
    }
  }
  for (auto x : stats) {
    cout << endl << x.first << " " << x.second;
  }
}


void analyse_article(string src){
  ifstream fin(src);
  string text( (istreambuf_iterator<char>(fin)),
      (istreambuf_iterator<char>()) );
  fin.close();
  ostream_iterator<string> owords(cout, " ");
  vector<string> tokens;
  get_tokens(text, delimiters, tokens);
  map<string, int> freqdi;
  get_type_freq(tokens, freqdi);
  set<string> keywords;
  get_x_freq_words(freqdi, 1, keywords);
  analyse_themes(keywords);
}
