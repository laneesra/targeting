#include "textstats.hpp"

int main()
{
	  ostream_iterator<string> owords(cout, " ");
	  DIR *dir;
	  struct dirent *entry;
	  struct stat buf;
	  dir = opendir("articles");
	  string s = "articles/";
	  while ((entry=readdir(dir)) != NULL) {
	    vector<string> inter;
	    string name = entry->d_name;
	    lstat(entry->d_name, &buf);
	    if (name.length()>2) {
	      string str = s+name;
		  cout << endl << endl <<  name.substr(0, name.length()-4);
	      analyse_article(str.c_str());
	    }
	  }
	return 0;
}
